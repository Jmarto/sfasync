<?php


namespace App\Message;


class AddPonkaToImage
{
    /**
     * @var int imagePostId
     */
    private $imagePostId;

    /**
     * AddPonkaToImage constructor.
     */
    public function __construct(int $imagePostId)
    {
        $this->imagePostId = $imagePostId;
    }

    /**
     * @return  int imagePostId
     */
    public function getImagePostId(): int
    {
        return $this->imagePostId;
    }
}
